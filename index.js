import { isString } from '@inkylabs/fannypack'
import latexIdx from '@inkylabs/latex-idx'
import {
  handleNode as htmlNode,
  replaceLabels
} from '@inkylabs/pulpweaver-html'
import chalk from 'chalk'
import childProcess from 'child_process'
import esDirname from 'es-dirname'
import { promises as fs } from 'fs'
import handlebars from 'handlebars'
import mime from 'mime-types'
import path from 'path'
import { v4 as genUuid } from 'uuid'

const __dirname = esDirname()
const TEMPLATE_DIR = path.join(__dirname, 'templates')

const red = m => console.error(chalk.red(m))

function padNum (n) {
  return String(n).padStart(4, '0')
}

function handleNode (node, ctx) {
  htmlNode(node, ctx)
}

const _templateCache = {}
async function readTemplate (name) {
  if (!(name in _templateCache)) {
    const templatePath = path.join(TEMPLATE_DIR, `${name}.handlebars`)
    const templateText = await fs.readFile(templatePath, 'utf8')
    _templateCache[name] = handlebars.compile(templateText)
  }
  return _templateCache[name]
}

function preparePages (pages, pagemap) {
  const cp = pages.map(p => Object.assign({}, p))
  cp.forEach(p => {
    p.spinefile = pagemap[p.page]
    if (p.spinefile) p.epubfile = `text/${pagemap[p.page]}`
  })
  cp
    .filter(p => !p.epubfile)
    .forEach(p => {
      switch (p.type) {
        case 'bibliography':
          p.spinefile = 'bibliography.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        case 'copyright_after':
        case 'copyright_before':
          p.spinefile = 'copyright.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        case 'dedication_after':
        case 'dedication_before':
          p.spinefile = 'dedication.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        case 'endorsements_after':
        case 'endorsements_before':
          p.spinefile = 'endorsements.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        case 'foreword_after':
        case 'foreword_before':
          p.spinefile = 'foreword.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        case 'index':
          p.spinefile = `i${padNum(p.index_number)}.xhtml`
          p.epubfile = 'text/' + p.spinefile
          break
        case 'listoffigures_before':
        case 'listoffigures':
        case 'listoffigures_after':
          p.spinefile = 'listoffigures.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        case 'halftitle_after':
        case 'halftitle_before':
        case 'title_after':
        case 'title_before':
          p.spinefile = 'title.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        case 'toc':
          p.spinefile = 'spinenav.xhtml'
          p.epubfile = 'text/' + p.spinefile
          break
        default:
          red(`Unhandled page type: ${p.type}`)
          console.error(p)
      }
    })
  return cp
}

async function readGen (filepath) {
  filepath = path.join('genfiles', `${filepath}.json`)
  const text = await fs.readFile(filepath, 'utf8')
  return JSON.parse(text)
}

async function writeFiles ({
  title,
  subtitle,
  authors,
  titlepage,
  copyright,
  dedication,
  endorsements,
  foreword,
  appendices,
  chapters,
  indices
}, {
  pages,
  cover
}, {
  refPrefix,
  idxs
}) {
  try {
    await fs.rmdir('EPUB', { recursive: true })
    await fs.rmdir('META-INF', { recursive: true })
  } catch (e) {
    if (e.code !== 'ENOENT') throw e
  }
  chapters = chapters
    .map((c, i) => ({
      filepath: c,
      index: i,
      paddedIndex: padNum(i + 1)
    }))
  appendices = appendices
    .map((c, i) => ({
      filepath: c,
      index: i + chapters.length,
      paddedIndex: padNum(i + 1 + chapters.length),
      appendix: true
    }))
  const usedsources = {}
  const cs = [
    ...chapters,
    ...appendices
  ]
  for (const c of cs) {
    const data = await readGen(c.filepath)
    c.epubfile = `c${c.paddedIndex}.xhtml`
    Object.assign(c, data)
    Object.assign(usedsources, data.usedsources)
    const m = pages.filter(p => p.tocentry === c.index)
    if (m.length) c.pagemark = m[0]
  }

  if (endorsements) endorsements = await readGen(endorsements)
  if (titlepage) titlepage = await readGen(titlepage)
  if (copyright) copyright = await readGen(copyright)
  if (dedication) dedication = await readGen(dedication)
  if (foreword) foreword = await readGen(foreword)
  const sources = Object.entries(usedsources)
    .sort()
    .map(([k, v]) => ({
      content: v
    }))
  const images = [
    endorsements,
    titlepage,
    copyright,
    dedication,
    foreword,
    ...cs
  ]
    .filter(c => !!c)
    .map(c => c.images)
    .flat()
    .map((img, i) => Object.assign(img, {
      id: padNum(i)
    }))
  indices = indices.map((x, i) => Object.assign({
    paddedIndex: padNum(i),
    pages: pages.filter(p => p.index_key === x.key),
    groups: idxs[x.key].groups
  }, x))
  const indexmap = Object.fromEntries(cs
    .map(c => c.indexEntries.map(id => [id, { epubfile: c.epubfile }]))
    .flat())
  indices.forEach(i => {
    i.groups.forEach(g => {
      g.entries.forEach(e => {
        e.instances.forEach(x => {
          if (x.id in indexmap) x.epubfile = indexmap[x.id].epubfile
        })
      })
    })
  })

  const pagemap = Object.fromEntries([
    ...endorsements.pages.map(p => [p, 'endorsements.xhtml']),
    ...titlepage.pages.map(p => [p, 'title.xhtml']),
    ...copyright.pages.map(p => [p, 'copyright.xhtml']),
    ...(dedication ? dedication.pages.map(p => [p, 'dedication.xhtml']) : []),
    ...(foreword ? foreword.pages.map(p => [p, 'foreword.xhtml']) : []),
    ...cs.map(c => c.pages.map(p => [p, c.epubfile]))
      .flat()
  ])

  const figures = cs
    .map(c => c.figures.map(f => Object.assign({
      epubfile: c.epubfile,
      chapterlabel: c.chapterlabel
    }, f)))
    .flat()

  const data = {
    title,
    subtitle,
    authors,
    titlepage,
    copyright,
    dedication,
    endorsements,
    foreword,
    chapters,
    appendices,
    indices,
    images,
    uuid: genUuid(),
    date: new Date().toISOString().replace(/\.\d{3}Z$/, 'Z'),
    sources,
    pages: preparePages(pages, pagemap),
    figures,
    coverImage: path.basename(cover),
    coverImageMimetype: mime.lookup(cover)

  }
  const t = async (p, o, d = data) => {
    const template = await readTemplate(p)
    const text = template(d)
    try {
      await fs.mkdir(path.dirname(o), { recursive: true })
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
    await fs.writeFile(o, text)
  }
  await t('mimetype', 'mimetype')
  await t('meta-inf-container.xml', 'META-INF/container.xml')
  await t('meta-inf-com.apple.ibooks.display-options.xml',
    'META-INF/com.apple.ibooks.display-options.xml')
  await t('epub-content.opf', 'EPUB/content.opf')
  await t('epub-nav.xhtml', 'EPUB/nav.xhtml')
  await t('epub-styles-main.css', 'EPUB/styles/main.css')
  await t('epub-text-title.xhtml', 'EPUB/text/title.xhtml')
  await t('epub-text-spinenav.xhtml', 'EPUB/text/spinenav.xhtml')
  await t('epub-text-bibliography.xhtml', 'EPUB/text/bibliography.xhtml')
  await t('epub-toc.ncx', 'EPUB/toc.ncx')
  if (endorsements) {
    await t('epub-text-endorsements.xhtml', 'EPUB/text/endorsements.xhtml')
  }
  if (copyright) {
    await t('epub-text-copyright.xhtml', 'EPUB/text/copyright.xhtml')
  }
  if (dedication) {
    await t('epub-text-dedication.xhtml', 'EPUB/text/dedication.xhtml')
  }
  if (foreword) {
    await t('epub-text-foreword.xhtml', 'EPUB/text/foreword.xhtml')
  }
  await t('epub-text-listoffigures.xhtml', 'EPUB/text/listoffigures.xhtml')
  for (const c of cs) {
    await t('epub-text-chapter.xhtml', `EPUB/text/c${c.paddedIndex}.xhtml`, c)
  }
  for (const i of indices) {
    await t('epub-text-index.xhtml', `EPUB/text/i${i.paddedIndex}.xhtml`, i)
  }

  for (const img of images.map(i => i.path)) {
    const dest = path.join('EPUB', 'figs', img)
    try {
      await fs.mkdir(path.dirname(dest), { recursive: true })
    } catch (e) {
      if (e.code !== 'EEXIST') throw e
    }
    await fs.copyFile(path.join('..', '..', img), dest)
  }
  const dest = path.join('EPUB', path.basename(cover))
  await fs.copyFile(path.join('..', '..', cover), dest)
}

async function zipEpub (outfile) {
  try {
    await fs.unlink(outfile)
  } catch (e) {
    if (e.code !== 'ENOENT') throw e
  }
  const proc = childProcess.spawn('zip', [
    '-D',
    '-r',
    '-0',
    '-X',
    outfile,
    'mimetype',
    'EPUB',
    'META-INF'
  ])
  proc.stdout.on('data', data => {
    const d = data.toString().trim()
    const color = chalk.yellow
    console.log(color(d))
  })
  proc.stderr.on('data', data => {
    const d = data.toString().trim()
    red(d)
  })

  return new Promise(resolve => proc.on('exit', resolve))
}

async function checkEpub (outfile) {
  const proc = childProcess.spawn('epubcheck', [
    outfile
  ])
  proc.stdout.on('data', data => {
    const d = data.toString().trim()
    const color = chalk.yellow
    console.log(color(d))
  })
  proc.stderr.on('data', data => {
    const d = data.toString().trim()
    red(d)
  })
  return new Promise(resolve => proc.on('exit', resolve))
}

export default async (config) => {
  config = Object.assign({
    outfile: `${config.name}.epub`,
    pages: []
  }, config)
  if (isString(config.pages)) {
    const json = await fs.readFile(config.pages, 'utf8')
    try {
      config.pages = JSON.parse(json)
    } catch (e) {
      if (e instanceof SyntaxError) {
        red(`Could not parse ${config.pages}: ${e}`)
        config.pages = []
      }
      throw e
    }
  }

  const idxs = await latexIdx.readConfig(config.indices)

  const globals = {
    refPrefix: genUuid(),
    idxs
  }
  let chapternum = 0
  let appendixnum = 64

  return {
    fileExt: 'json',
    remarkFilePlugin (opts) {
      this.Compiler = compiler

      const ctx = {
        chapterindex: opts.chapterindex,
        figures: [],
        footnotes: [],
        sources: opts.sources,
        usedsources: {},
        images: [],
        pages: [],
        refPrefix: globals.refPrefix,
        imagePrefix: path.join('..', 'figs'),
        idxs: idxs,
        indexEntries: [],
        allowPagemark: true,
        pagemarks: [],
        output: '',
        type: opts.type,
        chapternum,
        appendixnum,
        xhtml: true
      }
      function compiler (root, file) {
        ctx.file = file
        handleNode(root, ctx)
        chapternum = ctx.chapternum
        appendixnum = ctx.appendixnum
        let id
        switch (ctx.type) {
          case 'chapter':
            id = ctx.chapterlabel
              ? `c_${ctx.chapterlabel}`
              : `c_${ctx.title.toLowerCase()}`
            break
          case 'appendix':
            id = ctx.chapterlabel
              ? `a_${ctx.chapterlabel}`
              : `a_${ctx.title.toLowerCase()}`
            break
          default:
            id = ctx.type
        }
        return JSON.stringify({
          id,
          title: ctx.title,
          subtitle: ctx.subtitle,
          type: ctx.type,
          content: ctx.output,
          figures: ctx.figures,
          footnotes: ctx.footnotes,
          labels: Object.keys(ctx.labels),
          usedsources: ctx.usedsources,
          images: ctx.images,
          pages: ctx.pages,
          refPrefix: globals.refPrefix,
          indexEntries: ctx.indexEntries,
          chapterlabel: ctx.chapterlabel
        }, null, 2)
      }
    },
    async buildAfterFiles (bookConfig) {
      await replaceLabels(globals)
      await writeFiles(bookConfig, config, globals)
      const code = await zipEpub(config.outfile)
      await checkEpub(config.outfile)
      return !code
    }
  }
}
